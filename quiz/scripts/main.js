// connect submit button to listener
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

// connect clear button to listener
//var clearButton = document.getElementById('bsr-clear-button');
//clearButton.onmouseup = resetDictionaryInfo;

// initialize dictionaries
var guyDict = {
    "Siegfried": 0,
    "Keough": 0,
    "Knott": 0,
    "O'Neill": 0,
    "Stanford": 0,
    "Fisher": 0,
    "Alumni": 0,
    "St. Ed's": 0
};

var girlDict = {
    "Walsh": 0,
    "Welsh Fam": 0,
    "PW": 0,
    "Lewis": 0,
    "Farley": 0,
    "Ryan": 0,
    "Cavanaugh": 0,
    "Flaherty": 0,
    "PE": 0
};

//update label with dorm

function getFormInfo(name) {
    console.log("entered getFormInfo");

    //get name
    var name = document.getElementById('name-input').value;
    console.log("name is: " + name);


    // get answers from radio inputs
    var radio0 = document.getElementsByName('bsr-radios');
    var radio1 = document.getElementsByName('bsr-radios1');
    var radio2 = document.getElementsByName('bsr-radios2');
    var radio3 = document.getElementsByName('bsr-radios3');
    var radio4 = document.getElementsByName('bsr-radios4');
    var radio5 = document.getElementsByName('bsr-radios5');
    var radio6 = document.getElementsByName('bsr-radios6');
    var radio7 = document.getElementsByName('bsr-radios7');
    var radio8 = document.getElementsByName('bsr-radios8');
    var radio9 = document.getElementsByName('bsr-radios9');
    var radio10 = document.getElementsByName('bsr-radios10');

    // begin conditionals

    // gender
    var male;
    if (radio0[1].checked == true) {
        male = 0;
    }
    else{
        male = 1;
    }
    console.log("Male or Female: " + male);
    // Question 1: What college are you in?
    if (radio1[0].checked == true) {
        guyDict["Siegfried"] += 1;
        girlDict["Farley"] += 1;
    } else if (radio1[1].checked == true) {
        guyDict["Stanford"] += 1;
        girlDict["Cavanaugh"] += 1;
    } else if (radio1[2].checked == true) {
        guyDict["Alumni"] += 1;
        girlDict["Ryan"] += 1;
    } else {
        guyDict["St. Ed's"] += 1;
        girlDict["Flaherty"] += 1;
    }

    // Question 2: What is your favorite area of campus?
    if (radio2[0].checked == true) {
        guyDict["St. Ed's"] += 1;
        guyDict["Stanford"] += 1;
        girlDict["Farley"] += 1;
        girlDict["Cavanaugh"] += 1;
        girlDict["Lewis"] += 1;
    } else if (radio2[1].checked == true) {
        guyDict["Alumni"] += 1;
        guyDict["Fisher"] += 1;
        girlDict["Walsh"] += 1;
    } else if (radio2[2].checked == true) {
        guyDict["Knott"] += 1;
        guyDict["Siegfried"] += 1;
        girlDict["Flaherty"] += 1;
        girlDict["PW"] += 1;
        girlDict["PE"] += 1;
    } else {
        guyDict["Keough"] += 1;
        guyDict["O'Neill"] += 1;
        girlDict["Welsh Fam"] += 1;
    }

    // Question 3: What is your favorite flex point spot?
    if (radio3[0].checked == true) {
        guyDict["Keough"] += 1;
        girlDict["Ryan"] += 1;
    } else if (radio3[1].checked == true) {
        guyDict["St. Ed's"] += 1;
        girlDict["Walsh"] += 1;
    } else if (radio3[2].checked == true) {
        guyDict["O'Neill"] += 1;
        girlDict["Lewis"] += 1;
    } else {
        guyDict["Fisher"] += 1;
        girlDict["Cavanaugh"] += 1;
    }

    // Question 4: Favorite Landmark?
    if (radio4[0].checked == true) {
        guyDict["St. Ed's"] += 1;
        girlDict["Lewis"] += 1;
        girlDict["Walsh"] += 1;
    } else if (radio4[1].checked == true) {
        guyDict["Keough"] += 1;
        girlDict["Welsh Fam"] += 1;
    } else if (radio4[2].checked == true) {
        guyDict["Knott"] += 1;
        girlDict["Farley"] += 1;
    } else {
        guyDict["Fisher"] += 1;
        girlDict["PW"] += 1;
    }

    // Question 5: Dorm you Dislike?
    if (radio5[0].checked == true) {
        guyDict["Keough"] += 1;
        girlDict["Ryan"] += 1;
    } else if (radio5[1].checked == true) {
        guyDict["Siegfried"] += 1;
        girlDict["PW"] += 1;
    } else if (radio5[2].checked == true) {
        guyDict["Fisher"] += 1;
        girlDict["Walsh"] += 1;
    } else {
        guyDict["O'Neill"] += 1;
        girlDict["PE"] += 1;
    }

    // Question 6: Favorite Bar?
    if (radio6[0].checked == true) {
        guyDict["Siegfried"] += 1;
        girlDict["Farley"] += 1;
    } else if (radio6[1].checked == true) {
        guyDict["Stanford"] += 1;
        girlDict["Cavanaugh"] += 1;
    } else if (radio6[2].checked == true) {
        guyDict["Alumni"] += 1;
        girlDict["Ryan"] += 1;
    } else {
        guyDict["Knott"] += 1;
        girlDict["Welsh Fam"] += 1;
    }


    // Question 7: Favorite Mascot?
    if (radio7[0].checked == true) {
        guyDict["Keough"] += 1;
        girlDict["Farley"] += 1;
    } else if (radio7[1].checked == true) {
        guyDict["Knott"] += 1;
        girlDict["PE"] += 1;
    } else if (radio7[2].checked == true) {
        guyDict["Siegfried"] += 1;
        girlDict["PW"] += 1;
    } else {
        guyDict["Alumni"] += 1;
        girlDict["Lewis"] += 1;
    }

    // Question 8: Favorite On-campus tradition?
    if (radio8[0].checked == true) {
        guyDict["Keough"] += 1;
        girlDict["Cavanaugh"] += 1;
    } else if (radio8[1].checked == true) {
        guyDict["Siegfried"] += 1;
        girlDict["PW"] += 1;
    } else if (radio8[2].checked == true) {
        guyDict["Alumni"] += 1;
        girlDict["Walsh"] += 1;
    } else {
        guyDict["Fisher"] += 1;
        girlDict["Lewis"] += 1;
    }

    // Question 9: Go-to Dining Hall Meal?
    if (radio9[0].checked == true) {
        guyDict["Keough"] += 1;
        girlDict["Walsh"] += 1;
    } else if (radio9[1].checked == true) {
        guyDict["O'Neill"] += 1;
        girlDict["Ryan"] += 1;
    } else if (radio9[2].checked == true) {
        guyDict["St. Ed's"] += 1;
        girlDict["Lewis"] += 1;
    } else {
        guyDict["PE"] += 1;
        girlDict["Knott"] += 1;
    }

    // finished setting dictionary values
    console.log(guyDict);
    console.log(girlDict);


    // find the dorm with max value
    var dorm = "";
    var max = -1;
    var tempVal = 0;

    if (male == 1) {
        for (let key in guyDict) {
            tempVal = guyDict[key];
            if (tempVal > max) {
                max = tempVal;
                dorm = key;
            }
        }
    }

    // dorm = Object.keys(guyDict).reduce(function(a, b){ return guyDict[a] > guyDict[b] ? a : b });
    else {
        for (let key in girlDict) {
            tempVal = girlDict[key];
            if (tempVal > max) {
                max = tempVal;
                dorm = key;
            }
        }
    }
    console.log(dorm);
    displayDorm(dorm, name);

} // end of getFormInfo


function displayDorm(dorm, name) {
    console.log('entered displayDorm, dorm is ' + dorm + " and name is " + name);
    var dorm_body = document.getElementById('dorm-output');
    dorm_body.innerHTML = name + ", based on your answers, you belong in " + dorm + ".";
}
